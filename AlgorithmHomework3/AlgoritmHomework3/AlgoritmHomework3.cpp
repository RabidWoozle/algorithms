// AlgoritmHomework3.cpp : Defines the entry point for the console application.
//

/**
Author: Devon Belding
Date:7.27.2015


*/

#include "stdafx.h"
#include <stdlib.h>
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;
using std::vector;

int bin(int n, int k)
{
	if(k == 0 || n == k)
	{
		return 1;
	}
	else
		return bin(n-1, k-1) + bin(n-1, k);


}

int bin2(int n, int k)
{
	int i, j;



	vector< vector<int > > B(n+1, vector<int>(k+1));


	for(i = 0; i <= n; i++)
	{
		for(j=0; j <= std::min(i,k); j++)
		{
			if(j==0 || j==i)
			{
				B[i][j] = 1;

			}
			else
				B[i][j] = B[i-1][j-1] + B[i-1][j];
		}
	}

	int r = B[n][k];



	return r;
}


int _tmain(int argc, _TCHAR* argv[])
{
	cout << bin(5,2);

	cout << bin2(5,2);

	return 0;
}
