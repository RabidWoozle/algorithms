// PrimsAlgo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <limits.h>

#define vertex 10


using namespace std;

struct vertices
{
int index, destination, weight;
}v[vertex];



void prim(int n, const int W[][vertex], int edges[])
{
	int i, vnear;
	int min, e, x;
	int nearest[vertex];
	int distance[vertex];
	int visited[vertex];
	int aMatrix[vertex][vertex];

	for(i=0; i < n; i++)
	{
		visited[i] = 0;
		for(int j = 0; j<n;j++)
		{
			if(W[i][j] == 0 && i!=j)
				aMatrix[i][j] = 9999;
			else if(i==j)
				aMatrix[i][i] = 0;
		}
	}

	visited[0]=1;
	int k=0;


	while(k < n-1)
	{
		min = 9999;
		
		for(i=0; i< n; i++)
		{
			if(visited[i] == 1){
			for(int j=0; j<n;j++)
			{
				if(visited[j]!=1)
				{
					if(min > W[i][j] && W[i][j] > 0)
					{
						e = j;
						x=i;
						min = W[i][j];
						aMatrix[i][j] = min;
						aMatrix[j][i] = min;
					}
				}
			
			}
			}
			

		}
			v[k].index = x;
			v[k].destination = e;
			v[k].weight = min;

			visited[e] = 1;

			

			/*for(i=0; i < n; i++){
				if(W[i][vnear] < distance[vnear])
				{
					distance[i] = W[i][vnear];
					nearest[i] = vnear;
				}
			}*/
			
			k++;

	}

	for(i=0; i<n;i++)
			{
				for(int j = 0;j<n;j++ )
				{
					cout<<aMatrix[i][j]<<" ";
				}
				cout<<endl;
			}
}

int _tmain(int argc, _TCHAR* argv[])
{

	/**

			  [1]-----32*--[2]
			   |			|
			   |17*		    |45
	           |			|
		 18*   |			|
	[3]-------[4]----10*---[5]----28---[6]
	 |		   |		    |			|
	 |5* 	   |3*			|25			|6*
	 |    59   |			|			|
	[7]-------[8]-----4*---[9]----12*--[10]
	
	*/

	int W[vertex][vertex] =  {  {0,32, 0, 17, 0, 0, 0, 0, 0, 0},
							   {32, 0, 0, 0, 45, 0, 0, 0, 0, 0},
								{0, 0, 0, 18, 0, 0, 5, 0, 0, 0},
							   {17, 0,18, 0, 10, 0, 0, 3, 0, 0},
								{0,45, 0, 10, 0, 28, 0, 0,25, 0},
								{0, 0, 0, 0, 28, 0, 0, 0, 0, 6},
								{0, 0, 5, 0, 0, 0, 0, 59, 0, 0},
								{0, 0, 0, 3, 0, 0,59, 0, 4, 0},
								{0, 0, 0, 0,25, 0, 0, 4, 0,12},
								{0, 0, 0, 0, 0, 6, 0, 0,12, 0}};

	int e[vertex];

	prim(vertex, W, e);

	for( int k = 0; k < vertex-1; k++)
	{
		cout<<"source node:"<<v[k].index<<endl;
		cout<<"destination node:"<<v[k].destination<<endl;
		cout<<"weight of path:"<<v[k].weight<<endl;
	}

	
	return 0;
}

